# README
Assignment 2a and 2b
This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-linux]

* Rails version: Rails 5.0.2

* bootstrap-sass (3.3.6)

* bootstrap-will_paginate (0.0.11)

* Localhost: 8080

**Working**
Open the code in Cloud 9.

2. Go to the application’s directory. ( cd sample_app in my case )

3. gem install rails -v 5.0.2

3. Install gem files. ( $ bundle install )

4. Run rails server. ( $ rails server –b $IP –p $PORT )

5. Click on http://0.0.0.0:8080.

6. You will see this sample app page.

7. Click on sign up to create a new user profile.

8. Click on log in to log in to an existing user profile.

9. To log in as admin
   username:abhimike17@gmail.com
   password:Bshiva60@